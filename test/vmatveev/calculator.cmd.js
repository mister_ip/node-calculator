'use strict';

var child_process = require('child_process');

var positiveTests = [
	{ args: [17, 3, '+'], result: 20 },
	{ args: [15, 5, '-'], result: 10 },
	{ args: [10, 5, '/'], result: 2 },
	{ args: [7, 3, '*'], result: 21 }
];

var negativeTests = [
	{ args: [], title: 'args is empty array' },
	{ args: ['ss', 'res', '*'], title: 'args is not number', error: /Argument error/ }
];

describe('calculator command line test', function () {

	positiveTests.forEach(function (test) {
		it('operation: '+ test.args[test.args.length - 1]+ ' should work with default args' , function (done) {
			try {
				var execResult = execCalcSync(test.args);

				execResult.stderr.should.be.empty;
				parseFloat(execResult.stdout).should.eql(test.result);
			}
			catch (e) {
				return done(e);
			}

			done();
		});
	});

	negativeTests.forEach(function(test) {
		it('should return error when ' + test.title, function (done) {
			try {
				var execResult = execCalcSync(test.args);

				execResult.stdout.should.be.empty;
				execResult.stderr.should.match(test.error || /operation/);
			}
			catch (e) {
				return done(e);
			}

			done();
		})
	});

	//Async test
	it('should omit named args if both - named and default - are specified', function (done) {
		var command = 'node ./index.js 1 2 + --num1=3 --num2=5 --operation=+';

		child_process.exec(command, function(err, stdout, stderr) {
			if(err) {
				return done(err);
			}

			stderr.should.be.empty;
			parseFloat(stdout).should.be.eql(3);
			done();
		});
	})
});

function execCalcSync(args) {
	return child_process.spawnSync('node', ['./index.js'].concat(args), {encoding: 'utf8'});
}
